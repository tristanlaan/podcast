#!/usr/bin/env python3
# This program can download a podcast rss feed to easily backup a podcast.
# Copyright (c) 2020, 2021 Tristan Laan
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import defusedxml.ElementTree as ET
import urllib.request
import json
import multiprocessing
import mimetypes
import requests
import subprocess
import time
import random
import sys

from collections.abc import Iterator
from pathlib import Path
from multiprocessing import Process, Queue, Pool
from traceback import print_exc
from pathvalidate import sanitize_filename
from datetime import datetime
from dataclasses import dataclass
from xml.etree.ElementTree import Element as XMLElement


@dataclass(frozen=True)
class Config:
    """" Download configuration

    :param url: rss link to podcast
    :param dl_dir: directory to download items to
    :param dl_file: file containing already downloaded items to append to
    :param dl_threads: max amount of simultaneous downloads
    :param organization: Organization of podcast
    :param encode: True if items must be encoded, False if not
    :param enc_dir: output folder for encoded items
    :param enc_file: file containing already encoded items to append to
    :param enc_threads: max amount of simultaneous encodes
    :param ffmpeg: ffmpeg binary
    """
    url: str
    dl_dir: Path
    dl_file: Path
    dl_threads: int
    organization: str
    encode: bool = False
    enc_dir: Path | None = None
    enc_file: Path | None = None
    enc_threads: int = 0
    ffmpeg: str | None = None

def json_serial(obj) -> str:
    """JSON serializer for datetimes not serializable by default json code"""

    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError(f"Object of type {type(obj)} is not JSON serializable")

def init_download_worker() -> None:
    """Initialize worker thread by putting download in name"""
    process = multiprocessing.current_process()
    process.name = f'Download{process.name}'

def init_encode_worker() -> None:
    """Initialize worker thread by putting encode in name"""
    process = multiprocessing.current_process()
    process.name = f'Encode{process.name}'


def get_valid_filename(s: str | bytes) -> str:
    """Convert string to a valid filename

    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces and remove all illegal
    filename characters.
    >>> get_valid_filename("john's portrait: in 2004.jpg")
    'john's portrait in 2004.jpg'

    :param s: Filename to sanitize
    :return: Sanitized filename
    """
    s = str(s).strip()
    return sanitize_filename(s, platform='universal')


def parse_item(item: XMLElement) -> dict | None:
    """Get metadata from rss item

    :param item: XML item to parse
    :return: Dictionary with metadata of item, None on error
    """
    enc = item.find('enclosure')

    if enc is None:
        return None

    try:
        pubDate = datetime.strptime(item.findtext('pubDate'),
                                    r"%a, %d %b %Y %H:%M:%S %Z")
    except ValueError:
        pubDate = datetime.strptime(item.findtext('pubDate'),
                                    r"%a, %d %b %Y %H:%M:%S %z")

    return {
        'link': item.findtext('link'),
        'url': enc.attrib['url'],
        'type': enc.attrib['type'],
        'title': item.findtext('title'),
        'description': item.findtext('description'),
        'pubDate': pubDate
    }


def parse_xml(url: str) -> tuple[list[dict], str]:
    """Download and parse the RSS feed

    :param url: podcast URL
    :return: parsed items and url to podcast image
    """
    with urllib.request.urlopen(url) as f:
        xml = f.read().decode('utf-8')
    root = ET.fromstring(xml)
    img_url = root[0].find('image').findtext('url')
    items = [parse_item(child) for child in root[0] if child.tag == 'item']
    items.reverse()
    items = [item for item in items if item is not None]
    return items, img_url


def parse_downloaded(json_item: str) -> dict:
    """"Parse JSON and read iso datetime string as datetime
    :param json_item: JSON string of item
    :return: Decoded dictionary from JSON string
    """
    item = json.loads(json_item)
    item['pubDate'] = datetime.fromisoformat(item['pubDate'])
    return item


def filename(item: dict, extension: str | None = None) -> str:
    """Generate filename for podcast episode

    :param item: Podcast episode
    :param extension: Extension of file format
    :return: Filename
    """
    datestring = r"%Y-%m-%dT%H%M%S"
    if extension:
        return f"{item['pubDate'].strftime(datestring)} {item['title']}" \
               f"{extension}"
    return f"{item['pubDate'].isoformat()} {item['title']}"


def remove_downloaded(downloaded: list[dict], items: list[dict]) -> list[dict]:
    """Remove the already downloaded files from the items

    :param downloaded: List of already downloaded files
    :param items: List of items
    :return: List of items that are not yet downloaded
    """
    downloaded_titles = [item['title'] for item in downloaded]
    return [item for item in items if item['title'] not in downloaded_titles]


def find_non_encoded(downloaded: list[dict], encoded: list[str]) -> list[dict]:
    """Find items that are already downloaded but not yet encoded

    :param downloaded: List of downloaded items
    :param encoded:  List of filenames of encoded items
    :return: List of downloaded but not yet encoded items
    """
    return [item for item in downloaded if filename(item) not in encoded]


class DownloadWorker:
    def __init__(self, config: Config):
        self.config = config

    def download_item(self, item: dict) -> dict | None:
        """Download the item

        :param item: Item to download and encode
        :return: Item on success, None on failure
        """
        current = multiprocessing.current_process()
        print(f"Thread {current.name} started  downloading {filename(item)}",
              file=sys.stderr)

        time.sleep(random.uniform(0.1, 5))

        try:
            r = requests.get(item['url'])
            if r.status_code != 200:
                print('\033[1;31m' +
                      f"Thread {current.name} wrong status code "
                      f"{r.status_code}" + '\033[0m',
                      file=sys.stderr)
                return None

            podcast_file = self.config.dl_dir / get_valid_filename(
                filename(item, mimetypes.guess_extension(item['type'])))

            with podcast_file.open('wb') as f:
                f.write(r.content)
        except Exception as ex:
            print('\033[1;31m' +
                  f"Thread {current.name} failed downloading {filename(item)}:"
                  f" {type(ex).__name__}", file=sys.stderr)
            print_exc(file=sys.stderr)
            print('\033[0m', file=sys.stderr)
            return None

        print(f"Thread {current.name} finished downloading {filename(item)}",
              file=sys.stderr)
        return item


class EncodeWorker:
    def __init__(self, config: Config):
        self.config = config

    def encode_item(self, item: dict) -> dict | None:
        """Encode the item

        :param item: Item to encode
        :return: Item on success, None on failure
        """
        current = multiprocessing.current_process()
        print(f"Thread {current.name} started  encoding {filename(item)}",
              file=sys.stderr)

        old = self.config.dl_dir / get_valid_filename(
            filename(item, mimetypes.guess_extension(item['type'])))
        new = self.config.enc_dir / get_valid_filename(filename(item, ".ogg"))
        date = item['pubDate'].strftime(r"%Y-%m-%d")

        if 'music' in item['title'].lower():
            bitrate = "192k"
        else:
            bitrate = "96k"

        try:
            subprocess.run(
                [self.config.ffmpeg, "-y", "-i", old, "-c:a", "libvorbis",
                 "-b:a", bitrate, "-metadata", f"title={item['title']}",
                 "-metadata", f"date={date}", "-metadata",
                 f"contact={item['link']}", "-metadata",
                 f"organization={self.config.organization}", "-metadata",
                 f"description={item['description']}", new], check=True,
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except Exception as ex:
            print('\033[1;31m' +
                  f"Thread {current.name} failed encoding {filename(item)}: "
                  f"{type(ex).__name__}" + '\033[0m', file=sys.stderr)
            return None

        print(f"Thread {current.name} finished encoding {filename(item)}",
              file=sys.stderr)
        return item


def download_items(config: Config, items: list[dict],
                   queue: Queue) -> None:
    """Download all items and put them in encode queue

    :param config: Download configuration
    :param items: List of items to download
    :param queue: Queue to put downloaded items in
    :return: 0 on success, 1 on failure
    """
    dl_worker = DownloadWorker(config)
    with Pool(processes=config.dl_threads,
              initializer=init_download_worker) as pool:
        try:
            with config.dl_file.open('a') as f:
                for downloaded_item in pool.imap_unordered(
                        dl_worker.download_item, items):
                    if downloaded_item is not None:
                        print(json.dumps(downloaded_item,
                                         default=json_serial), file=f)
                        f.flush()
                        queue.put(downloaded_item)
        except KeyboardInterrupt:
            print("Terminating download workers", file=sys.stderr)
            pool.terminate()
            pool.join()
            queue.put(None)
            queue.close()

    queue.put(None)  # Send None to indicate end of queue
    queue.close()


def get_item_queue(queue: Queue) -> Iterator[dict]:
    """Generator to get items out of queue

    :param queue: Queue to get items from
    :return: Generator generating items from the queue
    """
    while True:
        item = queue.get()
        if item is None:  # Last item in queue
            return
        yield item


def encode_items(config: Config, queue: Queue) -> None:
    """Encode all items in queue

    :param config: Download configuration
    :param queue: Queue containing items to encode
    :return: 0 on success, 1 on failure
    """
    enc_worker = EncodeWorker(config)
    queue_getter = get_item_queue(queue)

    with Pool(processes=config.enc_threads,
              initializer=init_encode_worker) as pool:
        try:
            with config.enc_file.open('a') as f:
                for encoded_item in pool.imap_unordered(enc_worker.encode_item,
                                                        queue_getter):
                    if encoded_item is not None:
                        print(filename(encoded_item), file=f)
                        f.flush()
        except KeyboardInterrupt:
            print("Terminating encode workers", file=sys.stderr)
            pool.terminate()
            pool.join()
            queue.close()

    queue.close()


def start_process(config: Config) -> None:
    """Start

    :param config:
    """
    with config.dl_file.open() as f:
        downloaded: List[dict] = [parse_downloaded(line) for line in f]
    with config.enc_file.open() as f:
        encoded = f.read().splitlines()

    # Download podcast
    items, img_url = parse_xml(config.url)
    print(f"Total items: {len(items)}", file=sys.stderr)
    items = remove_downloaded(downloaded, items)
    print(f"New items:   {len(items)}", file=sys.stderr)
    if config.encode:
        enc_items = find_non_encoded(downloaded, encoded)
        print(f"Not yet encoded items:   {len(enc_items)}", file=sys.stderr)
        q = Queue()
    else:
        q = None

    dl_process = Process(target=download_items, name='download',
                         args=(config, items, q))
    if config.encode:
        enc_process = Process(target=encode_items, name='encode',
                              args=(config, q))
    else:
        enc_process = None

    dl_process.start()
    if config.encode:
        enc_process.start()

    dl_process.join()
    if config.encode:
        enc_process.join()


if __name__ == "__main__":
    import configparser
    import colorama
    from pathlib import Path

    colorama.init()

    print("""Copyright (c) 2020-2024 Tristan Laan
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
""", file=sys.stderr)

    # read config
    config_file = Path('config.ini')
    config = configparser.ConfigParser()
    config['podcast'] = {
        'url': r"https://url.to/rss?auth={auth}",
        'auth_required': True,
        'auth': "",
        'dl_list': "downloaded.txt",
        'output_folder': "Podcast/",
        'encode': True,
        'enc_list': "encoded.txt",
        'ffmpeg_bin': r"ffmpeg",
        'organization': "org",
        'dl_threads': 10,
        'enc_threads': 4
    }

    if not config_file.is_file():
        with open(config_file, 'w') as cfg:
            config.write(cfg)
        print(f"Config file created at: '{config_file.resolve()}'")
        exit(0)

    config.read(config_file)
    podcast_config = config['podcast']

    # Initialize program
    if podcast_config.getboolean('auth_required'):
        rss = podcast_config['url'].format(auth=podcast_config['auth'])
    else:
        rss = podcast_config['url']

    dl_list = Path(podcast_config['dl_list'])
    folder = Path(podcast_config['output_folder'])
    enc_list = Path(podcast_config['enc_list'])
    encoded = folder / 'encoded'

    if not dl_list.is_file():
        dl_list.touch()

    if not folder.is_dir():
        folder.mkdir(parents=True)

    if not enc_list.is_file():
        enc_list.touch()

    if not encoded.is_dir():
        encoded.mkdir(parents=True)

    config = Config(rss, folder, dl_list, podcast_config.getint('dl_threads'),
                    podcast_config['organization'],
                    encode=podcast_config.getboolean('encode'),
                    enc_dir=encoded, enc_file=enc_list,
                    enc_threads=podcast_config.getint('enc_threads'),
                    ffmpeg=podcast_config['ffmpeg_bin'])
    start_process(config)
