# Podcast downloader
This program can download an entire podcast from an RSS feed for archival
purposes. It can also automatically encode the audio files to save space.

## Installation
1. Download the program and enter folder

    ```
    git clone https://gitlab.com/tristanlaan/podcast.git
    cd podcast
    ```

2. Install requirements

    ```
    python3 -m pip install -r requirements.txt
    ```

3. Create config file
    ```
    ./podcast.py
    ```
4. Enter configuration in `config.ini`

    `url`: The url to the RSS feed. Use `{auth}` as a placeholder for an
    authentication token. \
    `auth_required`: `True` if an authentication token is required,
    otherwise `False`. \
    `auth`: The authentication token if required. \
    `dl_list`: File to keep track of the already downloaded files. \
    `output_folder`: Folder to save the audio files of the podcast in.
    `encode`: Whether the downloaded files must be encoded to save space.
    This will create a subfolder in `dl_list` named `encoded` containing the
    encoded files. **Note: this requires ffmpeg to be installed.** \
    `ffmpeg_bin`: Location of the ffmpeg binary. If you installed ffmpeg with
    apt this will most likely simply be `ffmpeg`. \
    `organization`: Organization of the podcast, to be entered in the metadata
    of encoded files. \
    `threads`: Max amount of simultaneous episodes that will be downloaded from
    the podcast.

## Running the program
```
./podcast.py
```
